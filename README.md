## How to run

-   `yarn install`
-   `yarn start`

*   `yarn test` to run tests (don't work because of some network error)
*   I would fix this error by running `yarn eject` to configure jest and enzyme myself, but this wouldn't have been in the scope of this challenge anymore.

## Features

*   render table view with all the scooters:
     sorting (default sorted by energy level asc)
     renders a colored bar which makes critical energy levels better visible
     includes paging

*   data gets updated every 10 seconds
*   switch to display a map instead of table supported by toggle
