import React from 'react'
import { energyColor } from '../utils'

describe('Marker', () => {
    it('returns a green marker color when energy level is above 66%', () => {
        const color = energyColor(67)
        expect(color).to.equals('#85cc00')
    })

    it('returns a yellow marker color when energy level is at 50%', () => {
        const color = energyColor(70)
        expect(color).to.equals('#ffbf00')
    })

    it('returns a red marker color when energy level is lower than 34%', () => {
        const color = energyColor(70)
        expect(color).to.equals('#ff2e00')
    })
})
