import React from 'react'
import ReactTable from 'react-table'
import 'react-table/react-table.css'

import EnergyLevel from './EnergyLevel'

const ScooterList = ({ scooters }) => (
    <ReactTable
        data={scooters}
        columns={[
            {
                Header: 'Fleet',
                columns: [
                    {
                        Header: 'Scooter',
                        accessor: 'license_plate',
                    },
                    {
                        Header: 'Energy Level',
                        accessor: 'energyLevel',
                        Cell: row => <EnergyLevel {...row} />,
                    },
                    {
                        Header: 'Distance',
                        accessor: 'distance_to_travel',
                    },

                    {
                        Header: 'Model',
                        accessor: 'model',
                    },
                    {
                        Header: 'ID',
                        accessor: 'id',
                    },
                ],
            },
        ]}
        defaultSorted={[
            {
                id: 'energyLevel',
                desc: false,
            },
        ]}
        defaultPageSize={20}
        className="-striped -highlight"
    />
)

export default ScooterList
