import React from 'react'

import CloseIcon from './CloseIcon'

const style = {
    container: {
        position: 'absolute',
        top: '50px',
        left: '10px',
        background: 'white',
        width: '350px',
        height: '40px',
    },
    model: {
        padding: 10,
        lineHeight: '40px',
        fontSize: 16,
        fontWeight: 200,
    },
    energyLevel: {
        padding: 10,
        lineHeight: '40px',
        fontSize: 16,
        fontWeight: 600,
    },
    iconWrapper: {
        position: 'absolute',
        top: 15,
        right: 10,
    },
}

const Scooter = ({ scooter, onClick }) => (
    <div style={style.container}>
        <span style={style.energyLevel}>{`${scooter.energyLevel}%`}</span>
        <span style={style.model}>{scooter.model}</span>

        <div className="close-icon" onClick={onClick} style={style.iconWrapper}>
            <CloseIcon />
        </div>
    </div>
)

export default Scooter
