import React from 'react'
import { energyColor } from '../utils'

const EnergyLevel = row => (
    <div
        style={{
            width: '100%',
            height: '100%',
            backgroundColor: '#dadada',
            borderRadius: '2px',
            position: 'relative',
        }}
    >
        <span
            style={{
                position: 'absolute',
                left: 5,
            }}
        >{`${row.value}%`}</span>
        <div
            style={{
                width: `${row.value}%`,
                height: '100%',
                backgroundColor: energyColor(row.value),
                borderRadius: '2px',
                transition: 'all .2s ease-out',
            }}
        />
    </div>
)

export default EnergyLevel
