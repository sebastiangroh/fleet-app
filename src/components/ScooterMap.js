import React, { Component } from 'react'
import GoogleMapReact from 'google-map-react'

import Marker from './Marker'
import Scooter from './Scooter'

const apiKey = 'AIzaSyC4R6AN7SmujjPUIGKdyao2Kqitzr1kiRg&v=3.exp&libraries=geometry,drawing,places"'
const berlinCoords = { lat: 52.520008, lng: 13.404954 }

class ScooterMap extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selectedScooter: null,
        }
    }

    render() {
        const { scooters } = this.props
        return (
            <div style={{ height: '92vh', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{
                        key: apiKey,
                    }}
                    defaultCenter={berlinCoords}
                    defaultZoom={15}
                >
                    {scooters.map(scooter => (
                        <Marker
                            lat={scooter.location.lat}
                            lng={scooter.location.lng}
                            onClick={() => {
                                this.setState({ selectedScooter: scooter })
                            }}
                            scooter={scooter}
                            key={scooter.id}
                        />
                    ))}
                    {this.state.selectedScooter && (
                        <Scooter
                            scooter={this.state.selectedScooter}
                            onClick={() => {
                                this.setState({ selectedScooter: null })
                            }}
                        />
                    )}
                </GoogleMapReact>
            </div>
        )
    }
}

export default ScooterMap
