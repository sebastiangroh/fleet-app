import React from 'react'
import { energyColor } from '../utils'

const Marker = ({ scooter, onClick }) => {
    const { energyLevel } = scooter

    return (
        <div
            style={{
                marginTop: 40,
                background: 'transparent',
                display: 'inline-block',
                borderRadius: '14px 14px 14px 0',
                width: 8,
                height: 8,
                border: `6px solid ${energyColor(energyLevel)}`,
                transform: 'rotate(-45deg)',
                position: 'relative',
                boxShadow: '-1px 1px 2px rgba(0,0,0,.2)',
                cursor: 'pointer',
            }}
            onClick={onClick}
        />
    )
}

export default Marker
