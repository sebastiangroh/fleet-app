import React, { Component } from 'react'

const scooterStore = WrappedComponent =>
    class ScooterStoreConnect extends Component {
        constructor(props) {
            super(props)

            this.state = {
                scooters: [],
            }
        }

        componentDidMount = () => {
            this.fetchData()

            // fetch every 10sec
            const fetchInterval = 10000
            this.timer = setInterval(this.fetchData, fetchInterval)
        }

        componentWillUnmount() {
            clearInterval(this.interval)
        }

        fetchData = () => {
            const url =
                'https://qc05n0gp78.execute-api.eu-central-1.amazonaws.com/prod/scooters'
            fetch(url)
                .then(response => response.json())
                .then(({ data }) => {
                    const { scooters } = data

                    this.setState({
                        scooters: scooters,
                    })
                })
        }

        render() {
            return (
                <div>
                    <WrappedComponent
                        scooters={this.state.scooters}
                        {...this.props}
                    />
                </div>
            )
        }
    }

export default scooterStore
