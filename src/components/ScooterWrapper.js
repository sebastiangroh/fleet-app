import React, { Component } from 'react'
import Toggle from 'react-toggle'

import scooterStore from './scooterStore'
import ScooterList from './ScooterList'
import ScooterMap from './ScooterMap'

import 'react-toggle/style.css'

class ScooterWrapper extends Component {
    constructor(props) {
        super(props)

        this.state = {
            showMap: false,
        }
    }

    render() {
        // correct naming and delete underscore for energyLevel
        const scooters = this.props.scooters.map(scooter => ({
            ...scooter,
            energyLevel: scooter.energy_level,
        }))

        return (
            <div>
                <div style={{ padding: 20 }}>
                    <Toggle
                        checked={this.state.showMap}
                        onChange={() => {
                            this.setState({ showMap: !this.state.showMap })
                        }}
                    />
                </div>

                {this.state.showMap ? (
                    <ScooterMap scooters={scooters} />
                ) : (
                    <ScooterList scooters={scooters} />
                )}
            </div>
        )
    }
}

export default scooterStore(ScooterWrapper)
