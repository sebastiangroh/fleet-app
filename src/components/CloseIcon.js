import React from 'react'

const CloseIcon = () => (
    <svg
        viewport="0 0 12 12"
        width="16px"
        height="16px"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <line x1="1" y1="11" x2="11" y2="1" stroke="black" strokeWidth="2" />
        <line x1="1" y1="1" x2="11" y2="11" stroke="black" strokeWidth="2" />
    </svg>
)

export default CloseIcon
