import React, { Component } from 'react'

import ScooterWrapper from './components/ScooterWrapper'

class App extends Component {
    render() {
        return <ScooterWrapper />
    }
}

export default App
