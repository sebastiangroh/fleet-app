export const energyColor = energyLevel =>
    energyLevel <= 33 ? '#ff2e00' : energyLevel <= 66 ? '#ffbf00' : '#85cc00'